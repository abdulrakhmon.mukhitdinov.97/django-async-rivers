# Django Async Rivers
A Django async view example on running external API calls concurrently.

Blog post: https://nextlinklabs.com/insights/django-async-views-improves-API-calls

## Setup
1. Clone repository
2. Create virtual environment and install `requirements.txt`
3. Run tests with: `python manage.py test`
4. Run server with: `uvicorn django_async_rivers.asgi:application --reload` 

Water data from the USGS Instantaneous Values Web Service
https://waterservices.usgs.gov/rest/IV-Service.html
