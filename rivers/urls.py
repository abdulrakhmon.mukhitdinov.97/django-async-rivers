from django.urls import path

from . import views


app_name = 'rivers'


urlpatterns = [
    path('dashboard/v1/', views.dashboard_v1, name='dashboard_v1'),
    path('dashboard/v2/', views.dashboard_v2, name='dashboard_v2'),
    path('dashboard/v3/', views.dashboard_v3, name='dashboard_v3'),
]
